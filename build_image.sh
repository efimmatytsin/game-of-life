#!/usr/bin/env bash

# variables
image_version=1.1;
image_name=game-of-life-backend;
jar_name=game-of-life-0.0.1-SNAPSHOT;

# scripts
mkdir ./deploy
cp ./target/${jar_name}.jar ./deploy
cp ./Dockerfile ./deploy
cd ./deploy
docker build -t ${image_name}:${image_version} --build-arg CONTAINER_VERSION_ARG=${image_version} .
docker tag ${image_name}:${image_version} svinokol/${image_name}:${image_version}
docker push svinokol/${image_name}:${image_version}
#docker save ${image_name}:${image_version} | gzip > ${image_name}-${image_version}.tar.gz