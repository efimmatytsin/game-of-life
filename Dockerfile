FROM java:8-jre
MAINTAINER Efim Matytsin

ADD ./game-of-life-0.0.1-SNAPSHOT.jar /app/
CMD ["java", "-Xmx400m", "-Dspring.profiles.active=prod", "-jar", "/app/game-of-life-0.0.1-SNAPSHOT.jar"]

EXPOSE 8080