package ru.mynewtons.gameoflife.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mynewtons.gameoflife.domain.Cell;
import ru.mynewtons.gameoflife.domain.Game;
import ru.mynewtons.gameoflife.repository.GameRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Service
public class GameServiceImpl implements ru.mynewtons.gameoflife.service.GameService {

    @Autowired
    private GameRepository gameRepository;

    @Override
    public Game save(Game game) {
       return gameRepository.save(game);
    }

    @Override
    public Optional<Game> findById(String id) {
        return gameRepository.findById(id);
    }


    @Override
    public Game calculateNextStep(Game game) {
        List<Cell> updatedCells = new ArrayList<>();
        for (long h = 0; h < game.getHeight(); h++) {
            for (long w = 0; w < game.getWidth(); w++) {
                int nr = game.neighboursCountAt(w, h);
                if (nr == 3) {
                    updatedCells.add(new Cell(w, h));
                } else if (nr == 2) {
                    Cell cell = new Cell(w, h);
                    if (game.getActiveCells().contains(cell)) {
                        updatedCells.add(cell);
                    }
                }
            }
        }
        game.setActiveCells(updatedCells);
        return game;
    }



}
