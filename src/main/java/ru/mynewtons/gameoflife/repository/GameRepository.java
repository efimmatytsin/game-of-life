package ru.mynewtons.gameoflife.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.mynewtons.gameoflife.domain.Game;

public interface GameRepository extends MongoRepository<Game, String> {

}
