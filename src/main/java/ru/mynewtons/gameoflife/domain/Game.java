package ru.mynewtons.gameoflife.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document(collection = "games")
public class Game {
    @Id
    private String id;
    private Long width;
    private Long height;
    private List<Cell> activeCells;
    private Boolean isActive;

    public int neighboursCountAt(long row, long col) {
        int sum = 0;

        if (isAlive(row - 1, col - 1)) {
            sum++;
        }

        if (isAlive(row - 1, col)) {
            sum++;
        }

        if (isAlive(row - 1, col + 1)) {
            sum++;
        }
        if (isAlive(row, col - 1)) {
            sum++;
        }

        if (isAlive(row, col + 1)) {
            sum++;
        }

        if (isAlive(row + 1, col - 1)) {
            sum++;
        }

        if (isAlive(row + 1, col)) {
            sum++;
        }

        if (isAlive(row + 1, col + 1)) {
            sum++;
        }

        return sum;
    }

    private boolean isAlive(long x, long y) {
        if(x < 0) {
            x = width - 1;
        }
        if(y < 0) {
            y = height - 1;
        }
        if(x == width) {
            x = 0;
        }
        if(y == height) {
            y = 0;
        }
        for (int i = 0; i < activeCells.size(); i++) {
            Cell cell = activeCells.get(i);
            if(cell.getX() == x && cell.getY() == y) {
                return true;
            }
        }
        return false;
    }
}
