package ru.mynewtons.gameoflife.controller;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.mynewtons.gameoflife.domain.Game;
import ru.mynewtons.gameoflife.exception.GameNotFoundException;
import ru.mynewtons.gameoflife.service.GameService;

import java.util.Optional;

@RestController
@RequestMapping("/api/game")
@Api(value = "game", description = "Game API")
@Slf4j(topic = "🦉Rest controller")
public class GameController {

    @Autowired
    private GameService gameService;


    @PostMapping("/")
    public ResponseEntity<Game> save(@RequestBody Game game){
        log.info("Found save request: " + game);
        return new ResponseEntity<>(gameService.save(game), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Game> findById(@PathVariable("id") String id) {
        log.info("Found findById request " + id);
        Optional<Game> optional = gameService.findById(id);
        if(!optional.isPresent()){
            throw new GameNotFoundException();
        }
        return new ResponseEntity<>(optional.get(), HttpStatus.OK);
    }

    @GetMapping("/{id}/next")
    public ResponseEntity<Game> doNextStep(@PathVariable("id") String id) {
        Optional<Game> optional = gameService.findById(id);
        if(!optional.isPresent()){
            throw new GameNotFoundException();
        }
        Game game = optional.get();
        game = gameService.calculateNextStep(game);

        return new ResponseEntity<>(gameService.save(game),HttpStatus.OK);
    }

}
